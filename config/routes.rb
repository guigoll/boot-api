require 'api_version_constraint'

Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      get 'categories/index'
    end
  end

   namespace :api, default:{format: :json}, constraints: { subdomain: 'api'}, path: '/' do
     namespace :v1,path: '/', constraints: ApiVersionConstraint.new(version: 1, default: true) do
       resources :users, only: [:index, :show, :create]
       resources :kinds, only: [:index, :show, :create]
       resources :categories, only: [:index ]
     end
   end
end
