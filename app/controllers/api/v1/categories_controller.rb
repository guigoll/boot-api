class Api::V1::CategoriesController < ApplicationController
  def index
    categories = Category.all

    render json: {category: categories}, status: 200
  end
end
