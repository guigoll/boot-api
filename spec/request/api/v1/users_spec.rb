require 'rails_helper'

RSpec.describe 'API user', type: :request do

 before { host! 'api.boot.test'}

 let(:headers) do {
   'Accept' => 'application/application.vnd.boot.v1',
   'Content-Type' =>  Mime[:json].to_s
 }
end

 describe "GET /users" do
   before do
      FactoryBot.create_list(:user,5)
      get '/users', params: {}, headers: headers
   end

   it 'return status code 200' do
      expect(response).to have_http_status(200)
   end

   it 'return 5 users from database' do
       expect(json_body[:user].count).to eq(5)
   end
 end

 describe "GET /users/:id" do
   let(:user){ FactoryBot.create(:user)}
   before{ get "/users/#{user.id}", params:{}, headers: headers}

   it 'return status code 200' do
      expect(response).to have_http_status(200)
   end

   it 'return json in the user' do
      expect(json_body[:name]).to eq(user.name)
   end
 end

 describe 'POST /users' do
   before do
      post '/users', params: {user: user_params}.to_json, headers: headers
   end

     context 'when the params are valid' do
       let(:user_params){FactoryBot.attributes_for(:user)}

       it 'return status code 201' do
          expect(response).to have_http_status(201)
       end

       it 'save in the database' do
          expect(User.find_by(name: user_params[:name])).not_to be_nil
       end

        it 'return the json cretae user' do
          expect(json_body[:name]).to eq(user_params[:name])
        end
     end

     context 'when the params are invalid' do
        let(:user_params){ FactoryBot.attributes_for(:user, name: '')}

        it 'return status code 422' do
          expect(response).to have_http_status(422)
        end
     end
 end
end
