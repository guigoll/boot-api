require 'rails_helper'

RSpec.describe 'API kinds', type: :request do

   before{host! 'api.boot.test'}

   let(:kind){FactoryBot.create(:kind)}

   let(:headers) do
     {
       'Accept' => 'application/application.vnd.boot.v1',
       'Content-Type' => Mime[:json].to_s
     }
   end

   describe 'GET /kinds' do
       before do
          FactoryBot.create_list(:kind, 5)
          get '/kinds', params: {}, headers: headers
       end

       it 'return status code 200' do
          expect(response).to have_http_status(200)
       end

       it 'return 5 kind ' do
          expect(json_body[:kind].count).to eq(5)
       end
   end

   describe 'GET /kinds/:id'  do
     let(:kind){ FactoryBot.create(:kind)}
     before{ get "/kinds/#{kind.id}", params:{}, headers: headers}

    it 'return status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'return json in the kind' do
      expect(json_body[:description]).to eq(kind.description)
    end
   end

   describe 'POST /kinds' do
     before do
       post '/kinds', params:{kind: kind_params}.to_json, headers: headers
     end

      context "when the params are valid" do
        let(:kind_params){ FactoryBot.attributes_for(:kind)}

        it 'return status code 201' do
          expect(response).to have_http_status(201)
        end

        it 'save in the database' do
          expect(Kind.find_by(description: kind_params[:description])).not_to be_nil
        end

        it 'return the json create kind' do
          expect(json_body[:description]).to eq(kind_params[:description])
        end
      end

      context "when the params are invalid" do
        let(:kind_params){ FactoryBot.attributes_for(:kind, description: '')}

        it 'return status code 422' do
           expect(response).to have_http_status(422)
        end

        it 'no save in the database' do
          expect(Kind.find_by(description: kind_params[:description])).to be_nil
        end

        it 'return json data  for errors' do
          expect(json_body).to have_key(:errors)
        end
      end
   end
end
