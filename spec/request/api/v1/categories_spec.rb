require 'rails_helper'

RSpec.describe  'API Category', type: :request do
  before{ host! 'api.boot.test'}
  let(:headers) do
    {
      'Accept' => 'application/vnd.boot.test.v1',
      'Content-Type' => Mime[:json].to_s
    }
  end

 describe "GET /Categories" do
   before do
     FactoryBot.create_list(:category, 5)
     get '/categories', params: {}, headers: headers
   end

   it 'return status code 200' do
      expect(response).to have_http_status(200)
   end

   it 'return 5 categories' do
     expect(json_body[:category].count).to eq(5)
   end
 end

end
