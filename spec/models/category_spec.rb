require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:category){ FactoryBot.build(:category)}

  it{ is_expected.to respond_to(:description)}
end
